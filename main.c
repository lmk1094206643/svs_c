#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "svs.h"
#include "unistd.h"

#define CERT_ID "sign01"
#define PRI_CERT_VALUE ""
#define PRI_KEY_INDEX 1

void svs_cert_test();
void svs_signdata_test();

SGD_HANDLE deviceHandle;
int main() {
    int rv;
    rv = SVS_OpenDevice(&deviceHandle);
    if (rv != 0)
        printf("SVS_OpenDevice fail %x\n", rv);
    svs_cert_test();
    svs_signdata_test();
    rv = SVS_CloseDevice(deviceHandle);
    if (rv != 0)
        printf("SVS_CloseDevice fail %x\n", rv);
}
void svs_cert_test(){
    SGD_HANDLE hsessionHandle;
    int rv;
    char certID[100] = {0};  //证书标识= CERT_ID;
    unsigned char certData[1200];
    int certDataLen = 1200;
    char certInfo[1000] = {0};
    int certInfoLen = 1000;
    int state;
    int status = 0;
    int i;
    int j;
    int certType[] = {CERT_PARSE_ITEM_START, SGD_CERT_VERSION, SGD_CERT_SERIAL, SGD_CERT_ISSUER, SGD_CERT_VALID_TIME,
                      SGD_CERT_SUBJECT, SGD_CERT_DER_PUBLIC_KEY, SGD_CERT_DER_EXTENSIONS, SGD_EXT_AUTHORITYKEYIDENTIFIER_INFO,
                      SGD_EXT_SUBJECTKEYIDENTIFIER_INFO, SGD_EXT_KEYUSAGE_INFO, SGD_EXT_PRIVATEKEYUSAGEPERIOD_INFO,
                      SGD_EXT_CERTIFICATEPOLICIES_INFO, SGD_EXT_POLICYMAPPINGS_INFO, SGD_EXT_BASICCONSTRAINTS_INFO,
                      SGD_EXT_POLICYCONSTRAINTS_INFO, SGD_EXT_EXTKEYUSAGE_INFO, SGD_EXT_CRLDISTRIBUTIONPOINTS_INFO,
                      SGD_EXT_NETSCAPE_CERT_TYPE_INFO, SGD_EXT_SELFDEFINED_EXTENSION_INFO, SGD_CERT_ISSUE_CN,
                      SGD_CERT_ISSUE_O, SGD_CERT_ISSUE_OU, SGD_CERT_SUBJECT_CN, SGD_CERT_SUBJECT_O, SGD_CERT_SUBJECT_OU,
                      SGD_CERT_SUBJECT_EMAIL, CERT_PARSE_ITEM_END};
    for (i = 0; i < 1; ++i) {
        memcpy(certID, CERT_ID, strlen(CERT_ID));
        memset(certData,0,sizeof (certData));
        certDataLen = 1200;
        rv = SVS_OpenSession(deviceHandle, &hsessionHandle);
        if (rv != 0){
            printf("SVS_OpenSession fail%x\n", rv);
        }
        rv = SVS_ExportCert(hsessionHandle, certID, certData,&certDataLen);
        if (rv != 0)
            printf("SVS_ExportCert fail %08x\n", rv);
        else
            printf("SVS_ExportCert sucess\n");
        for (j = 0; j < 28; ++j) {
            certInfoLen = 5000;
            SVS_ParseCert(hsessionHandle, certType[i], certData, certDataLen, certInfo, &certInfoLen);
            if (rv != 0)
                printf("SVS_ParseCert fail, %x\n", rv);
            else {
                status++;
            }
        }
        if (status == 28) {
            printf("SVS_ParseCert right\n");
        }
        rv = SVS_ValidateCert(hsessionHandle, certData, certDataLen, false, &state);
        if (rv != 0)
            printf("SVS_ValidateCert fail, rv:%08x\n", rv);
        else
            printf("SVS_ValidateCert right\n");

        rv = SVS_CloseSession(hsessionHandle);
        if (rv != 0)
            printf("SVS_CloseSession fail%x\n", rv);
    }

}

void svs_signdata_test() {
    int rv;
    SGD_HANDLE hsessionHandle;
    int signPrivateKeyIndex = PRI_KEY_INDEX;
    char signPrivateKeyValue[100] = PRI_CERT_VALUE;
    unsigned char signData[2048] = {0};
    int signDataLen = 2048;
    int type;
    char data[] = "1234567890*-abcdefghijklmnopqrstuvwxyz";
    int verifyLevel;
    unsigned char certData[1200];
    unsigned char exportcertData[1200];
    int certDataLen = 1200;
    char tmpData[1200] = {0};
    char certID[100] = {0};
    char certInfo[1500] = {0};
    int certInfoLen = 1500;
    int i;
    memcpy(certID, CERT_ID, strlen(CERT_ID));
    memset(tmpData, 0, sizeof (tmpData));
    for (i = 0; i < 1; ++i) {
        sleep(1);
        for (type = 1; type <= 3; ++type) {
            memset(certData, 0, sizeof (certData));
            memset(signData, 0, sizeof (signData));
            signDataLen = 5120;
            rv = SVS_OpenSession(deviceHandle, &hsessionHandle);
            if (rv != 0) {
                printf("SVS_OpenSession fail%x\n", rv);
            }
            switch (type) {
                case 1:
                    //证书
                    certDataLen = 1200;
                    rv = SVS_ExportCert(hsessionHandle, certID, exportcertData,&certDataLen);
                    if (rv != 0)
                        printf("SVS_ExportCert fail %08x\n", rv);
                    memcpy(certData,exportcertData,certDataLen);
                    break;
                case 2:
                    //证书序列号
                    certDataLen = 1200;
                    rv = SVS_ExportCert(hsessionHandle, CERT_ID, exportcertData,&certDataLen);
                    if (rv != 0)
                        printf("SVS_ExportCert fail %08x\n", rv);
                    certInfoLen = 1500;
                    rv = SVS_ParseCert(hsessionHandle, SGD_CERT_SERIAL, exportcertData, certDataLen, certInfo, &certInfoLen);
                    if (rv != 0)
                        printf("SVS_ParseCert fail, %x\n", rv);
                    memcpy(certData,certInfo,certInfoLen);
                    break;
                case 3:
                    //证书名，证书ID
                    memcpy(certData, CERT_ID, strlen(CERT_ID));
                    certDataLen = strlen(CERT_ID);
                    break;
                default:
                    break;

            }
            rv = SVS_SignData(hsessionHandle,SGD_SM3_SM2, signPrivateKeyIndex, signPrivateKeyValue, data,
                              strlen(data), signData, &signDataLen);
            if (rv != 0) {
                printf("SVS_SignData fail: %x\n", rv);
            } else {
                printf("SignData sucess\n");
            }
            for (verifyLevel = 0; verifyLevel < 3; verifyLevel++) {
                rv = SVS_VerifySignedData(hsessionHandle, type, (unsigned char *)certData, certDataLen,
                                          data, strlen(data), signData, signDataLen, verifyLevel);
                if (rv != 0) {
                    printf("SVS_VerifySignedData fail:%08x type:%d verifyLevel:%d\n", rv, type, verifyLevel);
                } else
                    printf("SVS_VerifySignedData sucess type:%d verifyLevel:%d\n", type, verifyLevel);
            }
            rv = SVS_CloseSession(hsessionHandle);
            if (rv != 0)
                printf("SVS_CloseSession fail%x\n", rv);
        }
    }
}