#include <stdbool.h>

#if defined(_WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) ||          \
    defined(__MINGW64__)
#ifdef NDSEC_SVS_EXPORT_API
#ifdef __GNUC__
#define NDSEC_SVS_EXPORT __attribute__((dllexport))
#else
#define NDSEC_SVS_EXPORT __declspec(dllexport)
#endif
#else
#ifdef __GNUC__
#define NDSEC_SVS_EXPORT __attribute__((dllimport))
#else
#define NDSEC_SVS_EXPORT __declspec(dllimport)
#endif
#endif
#else
#ifdef NDSEC_SVS_EXPORT_API
#define NDSEC_SVS_EXPORT __attribute__((visibility("default")))
#else
#define NDSEC_SVS_EXPORT
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* 常量标识 */
#define SGD_TRUE 0x00000001
#define SGD_FALSE 0x00000000

/* 证书解析项 */
#define CERT_PARSE_ITEM_START 0x00000000               // 证书解析项开始
#define SGD_CERT_VERSION 0x00000001                    // 证书版本
#define SGD_CERT_SERIAL 0x00000002                     // 证书序列号
#define SGD_CERT_ISSUER 0x00000005                     // 证书颁发者信息
#define SGD_CERT_VALID_TIME 0x00000006                 // 证书有效期
#define SGD_CERT_SUBJECT 0x00000007                    // 证书拥有者信息
#define SGD_CERT_DER_PUBLIC_KEY 0x00000008             // 证书公钥信息
#define SGD_CERT_DER_EXTENSIONS 0x00000009             // 证书扩展项信息
#define SGD_EXT_AUTHORITYKEYIDENTIFIER_INFO 0x00000011 // 颁发者密钥标示符
#define SGD_EXT_SUBJECTKEYIDENTIFIER_INFO 0x00000012 // 证书持有者密钥标识符
#define SGD_EXT_KEYUSAGE_INFO 0x00000013             // 密钥用途
#define SGD_EXT_PRIVATEKEYUSAGEPERIOD_INFO 0x00000014 // 私钥有效期
#define SGD_EXT_CERTIFICATEPOLICIES_INFO 0x00000015   // 证书策略
#define SGD_EXT_POLICYMAPPINGS_INFO 0x00000016        // 策略映射
#define SGD_EXT_BASICCONSTRAINTS_INFO 0x00000017      // 基本限制
#define SGD_EXT_POLICYCONSTRAINTS_INFO 0x00000018     // 策略限制
#define SGD_EXT_EXTKEYUSAGE_INFO 0x00000019           // 扩展密钥用途
#define SGD_EXT_CRLDISTRIBUTIONPOINTS_INFO 0x0000001A // CRL发布点
#define SGD_EXT_NETSCAPE_CERT_TYPE_INFO 0x0000001B    // netScape属性
#define SGD_EXT_SELFDEFINED_EXTENSION_INFO 0x0000001C // 私有的自定义扩展项
#define SGD_CERT_ISSUE_CN 0x00000021                  // 证书颁发者CN
#define SGD_CERT_ISSUE_O 0x00000022                   // 证书颁发者O
#define SGD_CERT_ISSUE_OU 0x00000023                  // 证书颁发者OU
#define SGD_CERT_SUBJECT_CN 0x00000031                // 证书拥有者信息CN
#define SGD_CERT_SUBJECT_O 0x00000032                 // 证书拥有者O
#define SGD_CERT_SUBJECT_OU 0x00000033                // 证书拥有者OU
#define SGD_CERT_SUBJECT_EMAIL 0x00000034 // 证书拥有者信息EMAIL
#define CERT_PARSE_ITEM_END 0x00000035    // 证书解析项结束

/*算法标识*/
#define SGD_SHA1_RSA 0x00010002
#define SGD_SHA256_RSA 0x00010004
#define SGD_SM3_SM2 0x00020201

/* 数据类型 */
typedef char SGD_CHAR;
typedef char SGD_INT8;
typedef short SGD_INT16;
typedef int SGD_INT32;
typedef long long SGD_INT64;
typedef unsigned char SGD_UCHAR;
typedef unsigned char SGD_UINT8;
typedef unsigned short SGD_UINT16;
typedef unsigned int SGD_UINT32;
typedef unsigned long long SGD_UINT64;
typedef unsigned int SGD_RV;
typedef void *SGD_OBJ;
typedef int SGD_BOOL;
typedef void *SGD_HANDLE;

/* 错误代码标识 */
#define GM_SUCCESS 0                       // 正常返回
#define GM_ERROR_BASE 0x04000000           // 错误码起始值
#define GM_ERROR_BASE_TSA 0x00100000       // TSA错误码起始值
#define GM_ERROR_CERT_ID 0x04000001        // 错误的证书标识
#define GM_ERROR_CERT_INFO_TYPE 0x04000002 // 错误的证书信息类型
#define GM_ERROR_SERVER_CONNECT 0x04000003 // 服务搭无法连接
#define GM_ERROR_SIGN_METHOD 0x04000004    // 签名算法类型错误
#define GM_ERROR_KEY_INDEX 0x04000005      // 签名者私钥索引值错误
#define GM_ERROR_KEY_VALUE 0x04000006   // 签名者私钥权限标识码错误
#define GM_ERROR_CERT 0x04000007        // 证书非法或服务器内不存在
#define GM_ERROR_CERT_DECODE 0x04000008 // 证书解码错误
#define GM_ERROR_CERT_INVALID_AF 0x04000009 // 证书过期
#define GM_ERROR_CERT_INVALID_BF 0x0400000A // 证书尚未生效
#define GM_ERROR_CERT_REMOVED 0x0400000B    // 证书已被吊销
#define GM_INVALID_SIGNATURE 0x0400000C     // 签名元效
#define GM_INVALID_DATA_FORMAT 0x0400000D   // 数据格式错误
#define GM_SYSTEM_FAILURE 0x0400000E        // 系统内部错误
// 0x0400000F~0x040000FF //预留
#define GM_ERROR_OPEN_DEVICE 0x04000010     // 打开设备失败
#define GM_ERROR_OPEN_SESSION 0x04000011    // 打开会话失败
#define GM_ERROR_GENERATE_RANDOM 0x04000012 // 生成随机数失败

#define ERR_USER 0x0400A000 // 自定义错误码起始值
#define GM_ERR_MALLOC_FAILURE (ERR_USER + 1)
#define GM_ERR_CERT_NO_EXTENSION (ERR_USER + 2)
#define GM_ERR_CERT_NO_NAME_ENTRY (ERR_USER + 3)
#define GM_ERR_NO_ISSUER_CERT (ERR_USER + 4)
#define GM_ERR_NO_SIGNER_ID (ERR_USER + 5)
#define GM_ERR_NO_SIGNER_PUBLIC_KEY (ERR_USER + 6)
#define GM_ERR_NO_SIGNER_SIGN_CERT (ERR_USER + 7)
#define GM_ERR_SIGN_METHOD (ERR_USER + 8)   // 签名算法类型错误
#define GM_ERR_DIGEST_METHOD (ERR_USER + 9) // 杂凑算法类型错误
#define GM_ERR_PARAMETER (ERR_USER + 10)
#define GM_ERR_KEY_TYPE (ERR_USER + 11) // 密钥类型
#define GM_ERR_ALG_TYPE (ERR_USER + 12) // 算法类型
#define GM_ERR_CERT_USAGE (ERR_USER + 13)
#define GM_ERR_RANDOM_FAILURE (ERR_USER + 14)
#define GM_ERR_SYSTEM_STATUS (ERR_USER + 15)
#define GM_ERR_NOT_ENOUGH_MEMORY (ERR_USER + 16)
#define GM_ERR_ENCRYPT_METHOD (ERR_USER + 17) // 加密算法类型错误
#define GM_ERR_NO_SIGNER_INFO (ERR_USER + 18)

#define GM_ERR_ACCESS_DENIED (ERR_USER + 21)
#define GM_ERR_WRITE_FAULT (ERR_USER + 22)

#define GM_ERR_DIGEST_FAILED (ERR_USER + 31)
#define GM_ERR_DIGEST_INIT_FAILED (ERR_USER + 32)
#define GM_ERR_DIGEST_UPDATE_FAILED (ERR_USER + 33)
#define GM_ERR_DIGEST_FINAL_FAILED (ERR_USER + 34)
#define GM_ERR_INVALID_DIGEST_LENGTH (ERR_USER + 35)

#define GM_ERR_SIGN_FAILED (ERR_USER + 41)
#define GM_ERR_VERIFY_FAILED (ERR_USER + 42)
#define GM_ERR_ENCRYPT_FAILED (ERR_USER + 43)
#define GM_ERR_DECRYPT_FAILED (ERR_USER + 44)

#define GM_ERR_KEY_PARSE_ERROR (ERR_USER + 51)
#define GM_ERR_CERT_PARSE_ERROR (ERR_USER + 52)
#define GM_ERR_CRL_PARSE_ERROR (ERR_USER + 53)
#define GM_ERR_PKCS7_PARSE_ERROR (ERR_USER + 54)
#define GM_ERR_PKCS12_PARSE_ERROR (ERR_USER + 55)
#define GM_ERR_URL_PARSE_ERROR (ERR_USER + 56)
#define GM_ERR_XML_PARSE_ERROR (ERR_USER + 57)
#define GM_ERR_JSON_PARSE_ERROR (ERR_USER + 58)
#define GM_ERR_NO_PARSE_ITEM_ERROR (ERR_USER + 59)

#define GM_ERR_CIPHER_INIT_FAILED (ERR_USER + 61)
#define GM_ERR_CIPHER_UPDATE_FAILED (ERR_USER + 62)
#define GM_ERR_CIPHER_FINAL_FAILED (ERR_USER + 63)
#define GM_ERR_INVALID_KEY_LENGTH (ERR_USER + 64)
#define GM_ERR_INVALID_IV_LENGTH (ERR_USER + 65)
#define GM_ERR_INVALID_IN_DATA_LENGTH (ERR_USER + 66)

#define GM_ERR_CERT_VERIFY_VALIDITY_DATA_FAILED (ERR_USER + 69)
#define GM_ERR_CERT_CHAIN_VERIFY_FAILED (ERR_USER + 70)
#define GM_ERR_CERT_SIGNATURE_VERIFY_FAILED (ERR_USER + 71)
#define GM_ERR_CERT_CRL_VERIFY_FAILED (ERR_USER + 72)

#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE (ERR_USER + 73)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE (ERR_USER + 74)
#define GM_ERR_CERT_VERIFY_IN_EFFECTIVE_CHAIN_STATE_FAILED (ERR_USER + 75)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE_CHAIN_STATE_SUCCESS (ERR_USER + 76)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE_CHAIN_STATE_FAILED (ERR_USER + 77)
#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE_CHAIN_STATE_SUCCESS (ERR_USER + 78)
#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE_CHAIN_STATE_FAILED (ERR_USER + 79)

#define GM_ERR_CERT_VERIFY_IN_EFFECTIVE_CHAIN_STATE_SUCCESS_IN_CRL             \
  (ERR_USER + 80)
#define GM_ERR_CERT_VERIFY_IN_EFFECTIVE_CHAIN_STATE_FAILED_NOT_IN_CRL          \
  (ERR_USER + 81)
#define GM_ERR_CERT_VERIFY_IN_EFFECTIVE_CHAIN_STATE_FAILED_IN_CRL              \
  (ERR_USER + 82)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE_CHAIN_STATE_SUCCESS_NOT_IN_CRL       \
  (ERR_USER + 83)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE_CHAIN_STATE_SUCCESS_IN_CRL           \
  (ERR_USER + 84)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE_CHAIN_STATE_FAILED_NOT_IN_CRL        \
  (ERR_USER + 85)
#define GM_ERR_CERT_VERIFY_LOSE_EFFECTIVE_CHAIN_STATE_FAILED_IN_CRL            \
  (ERR_USER + 86)
#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE_CHAIN_STATE_SUCCESS_NOT_IN_CRL        \
  (ERR_USER + 87)
#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE_CHAIN_STATE_SUCCESS_IN_CRL            \
  (ERR_USER + 88)
#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE_CHAIN_STATE_FAILED_NOT_IN_CRL         \
  (ERR_USER + 89)
#define GM_ERR_CERT_VERIFY_NOT_EFFECTIVE_CHAIN_STATE_FAILED_IN_CRL             \
  (ERR_USER + 90)

// Internal ERROR define
#define IERR_SUCCESS 0
#define IERR_BASE 0x10000000
#define IERR_INVALID_HANDLE (IERR_BASE + 1)
#define IERR_INVALID_PARAMETER (IERR_BASE + 2)
#define IERR_MALLOC_FAILURE (IERR_BASE + 3)
#define IERR_NOT_ENOUGH_MEMORY (IERR_BASE + 4)
#define IERR_INVALID_CARD_TYPE (IERR_BASE + 5)

#define IERR_INVALID_REQ_FORMAT (IERR_BASE + 10)
#define IERR_INVALID_REQ_VERSION (IERR_BASE + 11)
#define IERR_INVALID_REQ_TYPE (IERR_BASE + 12)
#define IERR_INVALID_REQ_TYPE_IMPL (IERR_BASE + 13)
#define IERR_INVALID_REQ_DATA (IERR_BASE + 14)

#define IERR_INVALID_RESP_FORMAT (IERR_BASE + 15)
#define IERR_INVALID_RESP_VERSION (IERR_BASE + 16)
#define IERR_INVALID_RESP_TYPE (IERR_BASE + 17)
#define IERR_INVALID_RESP_TYPE_IMPL (IERR_BASE + 18)
#define IERR_INVALID_RESP_DATA (IERR_BASE + 19)

#define IERR_DSO_LOAD_FAILURE (IERR_BASE + 20)
#define IERR_DSO_BIND_FAILURE (IERR_BASE + 21)
#define IERR_DSO_OPEN_DEVICE_FAILURE (IERR_BASE + 22)

#define IERR_NETWORK_INIT_FAILED (IERR_BASE + 30)
#define IERR_NETWORK_START_FAILED (IERR_BASE + 31)
#define IERR_NETWORK_INVALID_STATUS (IERR_BASE + 32)

// 算法标识

// 对称算法标识
#define SGD_SM1_ECB 0x00000101 ///< SM1 算法 ECB 加密模式
#define SGD_SM1_CBC 0x00000102 ///< SM1 算法 CBC 加密模式
#define SGD_SM1_CFB 0x00000104 ///< SM1 算法 CFB 加密模式
#define SGD_SM1_OFB 0x00000108 ///< SM1 算法 OFB 加密模式
#define SGD_SM1_MAC 0x00000110 ///< SM1 算法 MAC 加密模式

#define SGD_SSF33_ECB 0x00000201 ///< SSF33 算法 ECB 加密模式
#define SGD_SSF33_CBC 0x00000202 ///< SSF33 算法 CBC 加密模式
#define SGD_SSF33_CFB 0x00000204 ///< SSF33 算法 CFB 加密模式
#define SGD_SSF33_OFB 0x00000208 ///< SSF33 算法 OFB 加密模式
#define SGD_SSF33_MAC 0x00000210 ///< SSF33 算法 MAC 加密模式

#define SGD_SM4_ECB 0x00000401 ///< SMS4 算法 ECB 加密模式
#define SGD_SM4_CBC 0x00000402 ///< SMS4 算法 CBC 加密模式
#define SGD_SM4_CFB 0x00000404 ///< SMS4 算法 CFB 加密模式
#define SGD_SM4_OFB 0x00000408 ///< SMS4 算法 OFB 加密模式
#define SGD_SM4_MAC 0x00000410 ///< SMS4 算法 MAC 加密模式

#define SGD_ZUC_EEA3 0x00000801 ///< ZUC祖冲之机密性算法 128-EEA3算法
#define SGD_ZUC_EIA3 0x00000802 ///< ZUC祖冲之完整性算法 128-EIA3算法

// 非对称算法标识
#define SGD_RSA 0x00010000   ///< RSA 算法
#define SGD_SM2 0x00020100   ///< SM2 算法
#define SGD_SM2_1 0x00020200 ///< SM2_1 算法
#define SGD_SM2_2 0x00020400 ///< SM2_2 算法
#define SGD_SM2_3 0x00020800 ///< SM2_3 算法

// 杂凑算法标识
#define SGD_SM3 0x00000001    ///< SM3 算法
#define SGD_SHA1 0x00000002   ///< SHA1 算法
#define SGD_SHA256 0x00000004 ///< SHA256 算法
#define SGD_SHA512 0x00000008 ///< SHA512 算法
#define SGD_SHA384 0x00000010 ///< SHA384 算法
#define SGD_SHA224 0x00000020 ///< SHA224 算法
#define SGD_MD5 0x00000080    ///< MD5 算法

#define SGD_SM3_RSA 0x00010001
#define SGD_SHA1_RSA 0x00010002
#define SGD_SHA256_RSA 0x00010004
#define SGD_SM3_SM2 0x00020201

NDSEC_SVS_EXPORT int
SVS_OpenDeviceWithConfig(SGD_HANDLE *phDeviceHandle,
                         const unsigned char *pcDeviceConfig,
                         int pcDeviceConfigLength);

NDSEC_SVS_EXPORT int SVS_OpenDevice(SGD_HANDLE *hDeviceHandle);

NDSEC_SVS_EXPORT int SVS_CloseDevice(SGD_HANDLE hDeviceHandle);

NDSEC_SVS_EXPORT int SVS_OpenSession(SGD_HANDLE hDeviceHandle,
                                     SGD_HANDLE *hSessionHandle);

NDSEC_SVS_EXPORT int SVS_CloseSession(SGD_HANDLE hSessionHandle);

NDSEC_SVS_EXPORT int SVS_GenerateRandom(void *session_handle, int uiLength,
                                        unsigned char *pucRandom);

NDSEC_SVS_EXPORT int SVS_ExportCert(void *hSessionHandle, const char *certID,
                                    unsigned char *certData, int *certDataLen);
NDSEC_SVS_EXPORT int SVS_ParseCert(void *hSessionHandle, int certType,
                                   unsigned char *certData, int certDataLen,
                                   char *certInfo, int *certInfoLen);
NDSEC_SVS_EXPORT int SVS_ValidateCert(void *hSessionHandle,
                                      unsigned char *certData, int certDataLen,
                                      bool ocsp, int *state);
NDSEC_SVS_EXPORT int SVS_SignData(void *hSessionHandle, int method,
                                  int signPrivateKeyIndex,
                                  const char *signPrivateKeyValue,
                                  const char *data, int dataLen,
                                  unsigned char *signData, int *signDataLen);
NDSEC_SVS_EXPORT int SVS_VerifySignedData(void *hSessionHandle, int type,
                                          const unsigned char *certData,
                                          int certDataLen, const char *data,
                                          int dataLen, unsigned char *signData,
                                          int signDataLen, int verifyLevel);
NDSEC_SVS_EXPORT int SVS_SignDataInit(void *hSessionHandle, int method,
                                      const char *data, int dataLen,
                                      char *hashData, int *hashDataLen);
NDSEC_SVS_EXPORT int SVS_SignDataUpdate(void *hSessionHandle, int method,
                                        const char *hashMediantData,
                                        int hashMediantDataLen,
                                        const char *data, int dataLen,
                                        char *hashData, int *hashDataLen);
NDSEC_SVS_EXPORT int
SVS_SignDataFinal(void *hSessionHandle, int method, int signPrivateKeyIndex,
                  const char *signPrivateKeyValue, const char *hashMediantData,
                  int hashMediantDataLen, char *signData, int *signDataLen);
NDSEC_SVS_EXPORT int SVS_VerifySignedDataInit(void *hSessionHandle, int method,
                                              const char *data, int dataLen,
                                              char *hashData, int *hashDataLen);
NDSEC_SVS_EXPORT int
SVS_VerifySignedDataUpdate(void *hSessionHandle, int method,
                           const char *hashMediantData, int hashMediantDataLen,
                           const char *data, int dataLen, char *hashData,
                           int *hashDataLen);
NDSEC_SVS_EXPORT int SVS_VerifySignedDataFinal(
    void *hSessionHandle, int method, int type, const char *certData,
    int certDataLen, const char *hashMediantData, int hashMediantDataLen,
    const char *signData, int signDataLen, int verifyLevel);
NDSEC_SVS_EXPORT int
SVS_SignMessage(void *hSessionHandle, int method, int signPrivateKeyIndex,
                const char *signPrivateKeyValue, const char *data, int dataLen,
                unsigned char *signData, int *signDataLen, bool hashFlag,
                bool originalText, bool certificateChain, bool crl,
                bool authenticationAttributes);
NDSEC_SVS_EXPORT int
SVS_VerifySignedMessage(void *hSessionHandle, const char *data, int dataLen,
                        const unsigned char *signData, int signDataLen,
                        bool hashFlag, bool originalText, bool certificateChain,
                        bool crl, bool authenticationAttributes);
NDSEC_SVS_EXPORT int SVS_SignMessageInit(void *hSessionHandle, int method,
                                         const char *data, int dataLen,
                                         char *hashData, int *hashDataLen);
NDSEC_SVS_EXPORT int SVS_SignMessageUpdate(void *hSessionHandle, int method,
                                           const char *hashMediantData,
                                           int hashMediantDataLen,
                                           const char *data, int dataLen,
                                           char *hashData, int *hashDataLen);
NDSEC_SVS_EXPORT int SVS_SignMessageFinal(void *hSessionHandle, int method,
                                          int signPrivateKeyIndex,
                                          const char *signPrivateKeyValue,
                                          const char *hashMediantData,
                                          int hashMediantDataLen,
                                          char *signData, int *signDataLen);
NDSEC_SVS_EXPORT int SVS_VerifySignedMessageInit(void *hSessionHandle,
                                                 int method, const char *data,
                                                 int dataLen, char *hashData,
                                                 int *hashDataLen);
NDSEC_SVS_EXPORT int
SVS_VerifySignedMessageUpdate(void *hSessionHandle, int method,
                              const char *hashMediantData,
                              int hashMediantDataLen, const char *data,
                              int dataLen, char *hashData, int *hashDataLen);
NDSEC_SVS_EXPORT int SVS_VerifySignedMessageFinal(
    void *hSessionHandle, int method, const char *hashMediantData,
    int hashMediantDataLen, const char *signData, int signDataLen);

NDSEC_SVS_EXPORT int SVS_EncryptEnvelope(void *hSessionHandle, int type,
                                         const char *certID, const char *data,
                                         int dataLen, char *encData,
                                         int *encDataLen);
NDSEC_SVS_EXPORT int SVS_DecryptEnvelope(void *hSessionHandle, int type,
                                         const char *certID,
                                         const char *encData, int encDataLen,
                                         char *decData, int *decDataLen);

NDSEC_SVS_EXPORT int
SVS_SignMessageByCertId(void *hSessionHandle, const char *certID,
                        const char *keyValue, const char *data, int dataLen,
                        char *signData, int *signDataLen, bool hashFlag,
                        bool originalText, bool certificateChain, bool crl,
                        bool authenticationAttributes);
NDSEC_SVS_EXPORT int SVS_SignDataByCertId(void *hSessionHandle, int method,
                                          const char *certID,
                                          const char *keyValue,
                                          const char *data, int dataLen,
                                          char *signData, int *signDataLen);

NDSEC_SVS_EXPORT int SVS_SignAndEncryptEnvelope(void *hSessionHandle, int type,
                                                const char *signCertID,
                                                const char *encCertID,
                                                const char *data, int dataLen,
                                                int flag, char *encData,
                                                int *encDataLen);
NDSEC_SVS_EXPORT int SVS_DecryptAndVerifyEnvelope(
    void *hSessionHandle, int type, const char *encCertID, const char *data,
    int dataLen, char *decData, int *decDataLen, char *signCertData,
    int *signCertDataLen, char *encCertData, int *encCertDataLen);

#ifdef ENABLE_SVS_SEND_MESSAGE
NDSEC_SVS_EXPORT long SVS_SendMsg(unsigned char *asn1Req, long asn1ReqLen,
                                  unsigned char *asn1Resp);
#endif

#ifdef __cplusplus
}
#endif
